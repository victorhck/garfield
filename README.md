# garfield

Posiblemente piensas que no existe cosa más chula que poder disponer de un comic de Garfield cada vez que escribas «garfield» en tu terminal más cercana.

Es algo bonito e improductivo en lo que emplear tu tiempo.

Dependencias:
* curl
* wget
* feh

Es importante dejar una pequeña cabecera con algo de info sobre el script.

Es importante indicar la salida de wget para que sobrescriba y no genere otro archivo igual pero con doble extensión .1, .2, .3, ….

feh con -x no muestra bordes el visor de imágenes. Con -F lo que hace es ponerse en pantalla completa.

Script creado por Fanta: https://56k.es/fanta/el-comando-garfield-tira-comica-aleatoria-en-la-shell/

Descarga el script, ubícalo en algún sitio en tu $PATH y dale permisos de ejecución. ¡¡Y a disfrutar!!